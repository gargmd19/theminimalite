import logo from './logo.svg';
import unnamed from './elements/unnamed.jpg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={unnamed} className="App-logo" alt="logo" />
        <p>
          <code>Anime Girls OTW</code>
        </p>
        <p>
          Onii Chan
        </p>

      </header>
    </div>
  );
}

export default App;
